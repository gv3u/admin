--Solution so I don't have to type the functions 1 billion times.
--Please don't abuse it. :(
local EpicGamerFunctions = {}

local Cmdr = require(script.Parent)

function EpicGamerFunctions:getAsset(a)
	for _,v in pairs(Cmdr:GetStore("Assets")) do
		if a == v.Name then 
			return v:Clone()
		end
	end
end

function EpicGamerFunctions:Notify(who,title,msg,btn1)
		local notifyScript = EpicGamerFunctions:getAsset("Notify")
		notifyScript.Parent = who.Backpack
		notifyScript.msg.Value = msg
		notifyScript.title.Value = title
		notifyScript.btn1.Value = btn1
		notifyScript.Disabled = false
end

function EpicGamerFunctions:insertToStore(thing,store) -- My horrible solution.
	for _,v2 in pairs(thing) do
		if store == "Scripts" or store == "Assets" then
			table.insert(Cmdr:GetStore(store),v2:Clone())
		else
			table.insert(Cmdr:GetStore(store),v2)
		end
	end
end

function EpicGamerFunctions:isInTable(t,v3) -- An even worse solution to a stupid bug where the whitelist only works with one person.
	for _,v4 in pairs(t) do
		if v4 == v3 then
			return true;
		end
	end
	return false;
end


return EpicGamerFunctions