--[[
gv3u's Admin, a WIP Cmdr-based admin SS thing heavily inspired by Nebula's Stuff and MML admin.

Credits:
	The Cmdr devs for making the awesome Cmdr library.
	All the respective script devs for making the scripts in the scripts folder.

Changelog:
	The changelog has moved to https://gitlab.com/gv3u/admin
	That repo contains the changelog and the source code. Feel free to send pull requests.

]]--

-- Define stuff
script.Name = "gv3u's Admin | Bootstrap"
script.Parent = nil

local Commander = script:WaitForChild"Cmdr"
local CmdrClient = Commander.CmdrClient
local CmdrBackup = Commander:Clone()
local CmdrClientBackup = CmdrClient:Clone()
local Cmdr = require(Commander)
local ClientStuff = script["gv3u's Admin | Client"]:Clone()
local ChatBind = script["gv3u's Admin | Client"].ChatBind:Clone()
local RS = game:GetService("ReplicatedStorage")
local SSS = game:GetService("ServerScriptService")
local HS = game:GetService("HttpService")
local WhitelistData = HS:GetAsync("https://gitlab.com/gv3u/admin/-/raw/master/Whitelist.json") -- Don't ask me to whitelist you. PLEASE don't.
local Whitelist = HS:JSONDecode(WhitelistData)
local EGF = require(Commander.EpicGamerFunctions)


-- Load Cmdr
Commander.Parent = SSS
Commander.Name = "gv3u's Admin | Cmdr"
CmdrClient.Parent = RS
CmdrClient.Name = "gv3u's Admin | CmdrClient"

--Add Scripts
EGF:insertToStore(script.scripts:GetChildren(),"Scripts")
script.scripts:Destroy()

--Add Assets
EGF:insertToStore(script.assets:GetChildren(),"Assets")
script.assets:Destroy()

-- Register Stuff
EGF:insertToStore(Whitelist.Admins,"Admins")
EGF:insertToStore(Whitelist.Admins,"ScriptAccess")
EGF:insertToStore(Whitelist.Banland,"Banland")

-- register cmds
Cmdr:RegisterDefaultCommands() -- Built in Cmdr commands.
Cmdr:RegisterCommandsIn(script.cmds) -- My commands.

--[[
    Whitelist
    So as it turns out, as a surprise to LITERALLY NOBODY, I ####ed up badly. I put "return false" at the wrong spot in isInTable(). I'm such a dumb###.
	But then again, who cares? Nobody. Nobody will read this.
--]]

Cmdr.Registry:RegisterHook("BeforeRun", function(context)
	if not EGF:isInTable(context.Cmdr:GetStore("Admins"),context.Executor.UserId) then
		return "You don't have permission to run this command."
	end
end)

-- Prepare ClientStuff
local CS = ClientStuff:Clone()
CS.Parent = game:GetService('StarterPlayer').StarterPlayerScripts
CS.Disabled = false
for _,v4 in pairs(game:GetService('Players'):GetPlayers()) do
	local CS2 = CS:Clone()
	CS2.Parent = v4:WaitForChild('Backpack')
end
ChatBind.Parent = game:GetService("StarterPlayer").StarterCharacterScripts
ChatBind.Disabled = false

-- prevent cmdr from being destroyed
local function noRemove()
	while wait() do
		if CmdrClient.Parent ~= RS then
			CmdrClient = CmdrClientBackup:Clone()
			CmdrClient.Parent = RS
			CmdrClient.Name = "gv3u's Admin | CmdrClient"
		elseif Commander.Parent ~= SSS then
			Commander = CmdrBackup:Clone()
			Commander.Parent = RS
			Commander.Name = "gv3u's Admin | CmdrClient"
		end
		if CmdrClient.Name ~= "gv3u's Admin | CmdrClient" then
			CmdrClient.Name = "gv3u's Admin | CmdrClient"
		elseif Commander.Name ~= "gv3u's Admin | Cmdr" then
			Commander.Name = "gv3u's Admin | Cmdr"
		end
	end
end
coroutine.wrap(noRemove)()

local function checkForBans()
	while wait() do
		for _,v5 in pairs(Cmdr:GetStore("Banland")) do
			local p = game:GetService("Players"):GetPlayerByUserId(v5)
			if p then 
				p:Kick("You have been banned from this server.")
			end
		end
	end
end
coroutine.wrap(checkForBans)()

return nil;