# Admin Script
## "Somehow has worse code than YandereDev"

WIP ROBLOX Cmdr Admin Script inspired by Nebula's Stuff.  
The module can be found [here.](https://www.roblox.com/library/5524383080/gv3us-Admin)

# If you value your sanity, please do not read this code. It will make you lose faith in humanity and wish you never owned a computer.
# This repository serves more of an example of how NOT to write code.

# How to use (in game):
1. Go to any SS game or Script Builder.
2. Type in `require(5524383080)`.
3. Press Left Alt for the Cmdr GUI, or type commands in chat with the prefix `,`.

# How to use (inserting into a game)
This should be self-explanatory, but I'll describe it anyway.
1. Open ROBLOX Studio.
2. Create a script with the contents `require(5524383080)`
3. Place it in ServerScriptService
4. Publish