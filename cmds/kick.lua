return {
	Name = "kick";
	Aliases = {};
	Description = "Kicks a player or set of players.";
	Group = "Admin";
	Args = {
		{
			Type = "players";
			Name = "players";
			Description = "The player(s) to kick.";
		},
		{
			Type = "string";
			Name = "reason";
			Description = "Why are you kicking this player?";
			Optional = true;
		}
	};
}