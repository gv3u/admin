return {
	Name = "unrestrictscripts",
	Aliases = {"unrscripts","unrscripthub","allowscripts","allowscripthub"};
	Description = "Allows a player to run scripts.";
	Group = "Admin";
	Args = {
		{
			Type = "players",
			Name = "players",
			Description = "The player(s) to unrestrict.",
			Optional = true;
		}
	};
}