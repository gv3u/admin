return {
	Name = "time",
	Aliases = {};
	Description = "Sets ClockTime.";
	Group = "Admin";
	Args = {
		{
			Type = "number";
			Name = "time";
			Description = "The time to set.";
		}
	};
}