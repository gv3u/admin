return {
	Name = "invisff";
	Aliases = {"invisibleff"};
	Description = "the same thing as ff but it's invisible oooOOOO";
	Group = "Admin";
	Args = {
		{
			Type = "players";
			Name = "player(s)";
			Description = "The player(s) to give an invisible forcefield.";
			Optional = true;
		}
	};
}