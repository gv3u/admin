return {
	Name = "god2";
	Aliases = {"iddqd"};
	Description = "Gives you true god mode. Not even BreakJoints() can kill you.";
	Group = "Admin";
	Args = {
		{
			Type = "players";
			Name = "player(s)";
			Description = "The player(s) to give true god mode.";
			Optional = true;
		}
	};
}