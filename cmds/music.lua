return {
	Name = "music";
	Aliases = {"sound", "play", "moosic"};
	Description = "play some cool tunes";
	Group = "Admin";
	Args = {
		{
			Type = "string";
			Name = "music";
			Description = "The music to play.";
		}
	};
}