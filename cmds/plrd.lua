return {
	Name = "plrd";
	Aliases = {"bot","plrdummy"};
	Description = "Spawn a dummy with the apperance of another character.";
	Group = "Admin";
	Args = {
		{
			Type = "players";
			Name = "players";
			Description = "The player(s) to mimic the appearance of.";
			Optional = true;
		},
		{
			Type = "number";
			Name = "amount";
			Description = "How many dummies do you want?";
			Optional = true;
		},
	};
}
