return {
	Name = "ff";
	Aliases = {"forcefield"};
	Description = "What good is an admin script without this command?";
	Group = "Admin";
	Args = {
		{
			Type = "players";
			Name = "player(s)";
			Description = "The player(s) to give a forcefield.";
			Optional = true;
		}
	};
}