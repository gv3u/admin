return {
	Name = "give";
	Aliases = {"gogogadget", "getscript", "get", "load", "inject", "gimme"};
	Description = "Gets a script.";
	Group = "Admin";
	Args = {
		{
			Type = "string";
			Name = "script";
			Description = "The script to get.";
		},
		{
			Type = "players";
			Name = "players";
			Description = "The player(s) to give the script to.";
			Optional = true;
		},
	};
}