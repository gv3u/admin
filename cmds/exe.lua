return {
	Name = "exe";
	Aliases = {"script","s","c","run"};
	Description = "Execute a one-liner script.";
	Group = "Admin";
	Args = {
		{
			Type = "string";
			Name = "script";
			Description = "The script to execute.";
		}
	};
}