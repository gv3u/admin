return {
	Name = "laserpointer";
	Aliases = {"lpointer"};
	Description = "Finobe Cube??? 😳😳😳😳";
	Group = "Admin";
	Args = {
		{
			Type = "players";
			Name = "player(s)";
			Description = "The player(s) to give the Laser Pointer.";
			Optional = true;
		}
	};
}