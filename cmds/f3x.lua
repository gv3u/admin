return {
	Name = "f3x";
	Aliases = {"btools"};
	Description = "Gives a player F3X.";
	Group = "Admin";
	Args = {
		Type = "players";
		Name = "player(s)";
		Description = "The player(s) to give F3X.";
		Optional = true;
	};
}