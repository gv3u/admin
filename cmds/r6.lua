return {
	Name = "r6";
	Aliases = {};
	Description = "Converts a player's character to R6.";
	Group = "Admin";
	Args = {
		{
			Type = "players";
			Name = "players";
			Description = "The player(s) to convert to R6.";
			Optional = true;
		},
	};
}
