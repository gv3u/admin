return function(context,players)
	local EGF = require(game:GetService("ServerScriptService")["gv3u's Admin | Cmdr"].EpicGamerFunctions)
	
	local plr;
	if not players then 
		for _,v in pairs(game:GetService("Players"):GetPlayers()) do
			plr = v
		end
	else
		for _,p in pairs(players) do
			plr = p
		end
	end
	
	local scriptAccess = context.Cmdr:GetStore("ScriptAccess")
	local plrid = plr.UserId
	if EGF:isInTable(scriptAccess,plrid) and not EGF:isInTable(context.Cmdr:GetStore("Admins"),plrid) then
		scriptAccess[plrid] = nil
		EGF:Notify(plr,"gv3u's Admin","You have been revoked access to the Script Hub.","GO TO HELL")
	end
end