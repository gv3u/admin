return {
	Name = "ungod";
	Aliases = {"uniddqd"};
	Description = "Removes god mode.";
	Group = "Admin";
	Args = {
		{
			Type = "players";
			Name = "player(s)";
			Description = "The player(s) to remove god mode";
			Optional = true;
		}
	};
}