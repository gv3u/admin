return function(context, num)
	if not num then num=1 end
	for i=1,num do
		local dummy = require(game:GetService("ServerScriptService")["gv3u's Admin | Cmdr"].EpicGamerFunctions):getAsset("Dummy")
		dummy.Parent = workspace
		dummy.Torso.CFrame = context.Executor.Character.HumanoidRootPart.CFrame
	end
	return ("Spawned %d dummies."):format(num)
end