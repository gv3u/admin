return function(context)
	local moosic = workspace:WaitForChild("gv3u's Admin | Music")
	if not moosic then
		return 'No music is currently playing.'
	end
	moosic:Destroy()
	return "Music stopped."
end