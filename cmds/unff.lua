return {
	Name = "unff";
	Aliases = {"unforcefield"};
	Description = "Removes a player's forcefield.";
	Group = "Admin";
	Args = {
		{
			Type = "players";
			Name = "player(s)";
			Description = "The player(s) to remove their forcefield.";
			Optional = true;
		}
	};
}