return function(context, sn, players)
	local function getScript(scr)
		for _,s in pairs(context.Cmdr:GetStore("Scripts")) do
			if s.Name:lower() == scr:lower() then
				return s:Clone()
			end
		end
	end
	
	local s = getScript(sn)
	
	local plr;
	if not players then 
		plr = context.Executor
	else
		for _,p in pairs(players) do
			plr = p
		end
	end
	
	if s and plr.Character then
		s.Parent = plr.Character
		for _,a in pairs(s:GetChildren()) do
			if a.Name:lower() == "owner" then
				if a:IsA("StringValue") then
					a.Value = plr.Name
				elseif a:IsA("ObjectValue") then
					a.Value = plr
				end
			elseif a.Name:lower() == "usesezconvert" and a.Value == true then -- It ain't much, but it's honest work.
   				local EzConvert = require(game:GetService("ServerScriptService")["gv3u's Admin | Cmdr"].EpicGamerFunctions):getAsset("EzConvert")
    			EzConvert.Parent = s
				a:Destroy()
			end
		end
		if s.Disabled == true then 
			s.Disabled = false
		end
		return "Script given."
	elseif not plr.Character then
		return "Character has not spawned."
	else
		return "Invalid script name."
	end

end