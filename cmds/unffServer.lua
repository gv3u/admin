return function(context, plrs)
	if not plrs then 
		plr = context.Executor
	else
		for _, p in pairs(plrs) do
			plr = p
		end
	end
	
	if plr.Character then
		local ff = plr.Character:FindFirstChildOfClass("ForceField")
		ff:Destroy()
	end
	
end