return function (context, players, reason)
	if not reason then 
		reason = "No reason specified." 
	end
	for _, player in pairs(players) do
		if player == context.Executor then
			return "Seriously? Are you THAT depressed that you want to kick yourself?"
		end
		player:Kick("You have been kicked for the following reason: " .. reason)
		return ("Kicked %d players."):format(#players)
	end
end