return function(context, plrs)
	local plr;
	if not plrs then 
		plr = context.Executor
	else
		for _, p in pairs(plrs) do
			plr = p
		end
	end
	
	local funnyCube = require(game:GetService("ServerScriptService")["gv3u's Admin | Cmdr"].EpicGamerFunctions):getAsset("Laser Pointer")
	funnyCube.Parent = plr.Backpack
	funnyCube.MainScript.Disabled = false
end