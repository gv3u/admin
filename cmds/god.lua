return {
	Name = "god";
	Aliases = {};
	Description = "Gives you god mode (NOTE: Does not protect against BreakJoints()).";
	Group = "Admin";
	Args = {
		{
			Type = "players";
			Name = "player(s)";
			Description = "The player(s) to give god mode.";
			Optional = true;
		}
	};
}