return function(context,players)
	local EGF = require(game:GetService("ServerScriptService")["gv3u's Admin | Cmdr"].EpicGamerFunctions)
	
	local plr;
	if not players then 
		for _,v in pairs(game:GetService("Players"):GetPlayers()) do
			plr = v
		end
	else
		for _,p in pairs(players) do
			plr = p
		end
	end
	
	local plrid = plr.UserId
	local scriptAccess = context.Cmdr:GetStore("ScriptAccess")
	if not EGF:isInTable(scriptAccess,plrid) then
		table.insert(scriptAccess,plrid)
		EGF:Notify(plr,"gv3u's Admin","You have been allowed access to the Script Hub.","Cool.")
	end
end