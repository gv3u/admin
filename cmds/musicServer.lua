-- todo: cool visualization stuff
-- also this doesn't work even though it SHOULD, once again proving I'm worse than yanderedev
return function(context, id)
	local MPS = game:GetService("MarketplaceService")
	local GPI = MPS:GetProductInfo()
	local name = GPI(tonumber(id)).Name
	--check if music is already playing, and if it is, stop it
	if workspace:FindFirstChild("gv3u's Admin | Music") then
		local m = workspace:FindFirstChild("gv3u's Admin | Music")
		m:Destroy()
	end
	--create cool music
	local moosic = Instance.new("Sound",workspace)
	moosic.Name = "gv3u's Admin | Music"
	moosic.SoundId = 'rbxassetid://'..id
	
	return "Now playing "..name
end