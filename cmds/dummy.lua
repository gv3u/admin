return {
	Name = "dummy";
	Aliases = {"d"};
	Description = "Spawn some dummies.";
	Group = "Admin";
	Args = {
		{
			Type = "number";
			Name = "amount";
			Description = "How many dummies do you want?";
			Optional = true;
		}
	};
}