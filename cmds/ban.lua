return {
	Name = "ban";
	Aliases = {};
	Description = "Bans a player or set of players.";
	Group = "Admin";
	Args = {
		{
			Type = "players";
			Name = "players";
			Description = "The player(s) to ban.";
		},
		{
			Type = "string";
			Name = "reason";
			Description = "Why are you banning this player?";
			Optional = true;
		}
	};
}