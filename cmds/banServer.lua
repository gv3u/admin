return function (context, players, reason)
	--Set default reason.
	if not reason then 
		reason = "No reason specified." 
	end
	for _, player in pairs(players) do
		--Funny witty message.
		if player == context.Executor then
			return "Look, I know life is tough. I've been there, trust me. You have to understand this world has toxic people."
		end
		--Kicks the player then adds them to the Banland table.
		player:Kick("You have been banned for the following reason: " .. reason)
		table.insert(context.Cmdr:GetStore("Banland"),player)
		--Banning complete.
		return ("Banned %d players."):format(#players)
	end
end