return {
	Name = "restrictscripts",
	Aliases = {"rscripts","blockscripts","rscripthub","blockscripthub"};
	Description = "Restrict the Script Hub to Admins only.";
	Group = "Admin";
	Args = {
		{
			Type = "players",
			Name = "players",
			Description = "The player(s) to restrict.",
			Optional = true;
		}
	};
}