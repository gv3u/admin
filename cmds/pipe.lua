return {
	Name = "pipe";
	Aliases = {};
	Description = "...";
	Group = "Admin";
	Args = {
		{
			Type = "players";
			Name = "player(s)";
			Description = "The player(s) to send to the [REDACTED] cave.";
		}
	};
}