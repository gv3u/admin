return function(context,plrs)
	local plr;
	if not plrs then 
		plr = context.Executor
	else
		for _, p in pairs(plrs) do
			plr = p
		end
	end
    
	--Check if character exists.
	if not plr.Character then
		return "Character does not exist or has not spawned yet."
	end
	--Prepare the R6 dummy.
	local dummy = require(game:GetService("ServerScriptService")["gv3u's Admin | Cmdr"].EpicGamerFunctions):getAsset("R6Dummy")
	dummy.HumanoidRootPart.CFrame = plr.Character.HumanoidRootPart.CFrame
	--Set the R6 dummy's appearance to that of the player's.
	local charAppearance = game:GetService("Players"):GetCharacterAppearanceAsync(plr.UserId)
	for _,stuff in pairs(charAppearance:GetChildren()) do
		if not stuff:IsA("Folder") and not stuff:IsA("NumberValue") and not stuff:IsA("BoolValue") and not stuff:IsA("Decal") then -- Only copy the appearance.
			stuff.Parent = dummy
		end
	end
	--Set Face ID.
	dummy.Head:WaitForChild("face").Texture = charAppearance.face.Texture
	--Change the character to the R6 dummy.
	dummy.Name = plr.Name
	plr.Character = dummy
	--Enable R6 dummy scripts.
	for _,thingies in pairs(dummy:GetChildren()) do
		if thingies:IsA("Script") then
			thingies.Disabled = false
		end
	end
	--Set the R6 dummy parent to workspace.
	dummy.Parent = workspace
	--R6 conversion complete.
	return "Successfully converted into R6."
end