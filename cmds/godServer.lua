return function(context, plrs)
	if not plrs then 
		plr = context.Executor
	else
		for _, p in pairs(plrs) do
			plr = p
		end
	end
	
	if plr.Character then
		local H = plr.Character:FindFirstChildOfClass("Humanoid")
		H.Health = math.huge
		H.MaxHealth = math.huge
	end
	
end