wait(.5)

--Enable chat commands
local ChatBind = script:WaitForChild("ChatBind")
ChatBind.Parent = script.Parent
ChatBind.Disabled = false

--Enable command bar/terminal
script.Parent = nil
local CmdrClient = require(game:GetService("ReplicatedStorage")["gv3u's Admin | CmdrClient"])

CmdrClient:SetPlaceName"gv3u's Admin" -- Set "hostname" of the "terminal"
CmdrClient:SetActivationKeys({Enum.KeyCode.LeftAlt}) -- Set left alt key as the "terminal" toggle button thing

local messages = {
	"bruh",
	"Okay, this is epic.",
	"how do I load gv3u admin it's not working",
	"guys I ran the funny toad (character from mario) script for the 1000th time this week please laugh",
	"hail citizen",
	"music makes you lose control",
	"Totally not a Nebula's Stuff ripoff™®.",
	"Based? Based on what?",
	"Serversides were a mistake.",
	"roblox voidacity script builder free script download 360p working no virus",
	"Me when when tfw thw face when tfw when you the when the tfw",
	"Are ya coding son?",
	"is self",
	"oh yeah woo yeah",
	"Hey guys, gv3u from the video game ROBLOX here to explain this epic may-may presented before you. So basically, this meme is saying that we're a horrible species, and that we're driving ourselves to extinction. gv3u out!",
	"Yeah yeah, I totally get that. But did you know my computer has Gentoo Linux with Libreboot installed on it?",
	"Do you are have stupid?",
	"I swear, if you ask me to whitelist you, I will",
	[["Somehow has worse code than YandereDev"]],
	"NO I did NOT LOAD GV ADMIN DELETE THIS OFF MY FACEBOOK PAGE THIS INSTANT",
	":jill gv3u",
	"You've roasted your last toad.",
	"Is this what happens when",
	"Is humanity really going to",
	"how do you get gv3u admin it sort of looks like a hacker linux terminal how do you get it",
	"Fun Fact: gv3u's Obby 3 will never come out.",
	"Fun Fact: Did you know that this is not a sentence or a question?",
	"Fun Fact: bruh",
	"Nebula's Stuff²",
	"These jokes were totally not stolen from the 2b2t MOTDs.",
	"Type ,ec to bypass FE.",
	"You're not a real gamer, unless you're using Gentoo with i3-gaps.",
	"Welcome to Yandere Simulator. After 3 milleniums in development, hopefully it will have been worth the wait.",
	"Do you ever just",
	"Now with more atrocious code!",
	"what",
	"Worse than Nebula's Stuff!",
	"SUPER. HOT. SUPER. HOT. SUPER. HOT.",
	"MIND IS SOFTWARE. BODIES ARE DISPOSABLE."
}

local seriousMessages = {
	"I just want to be happy.",
	"I just want to be respected.",
	"Happiness is temporary. Pain is eternal.",
	"It's an endless cycle. I start a project, get lazy, and kill it off before it even hits beta.",
	"Does anyone even read this?",
	"...",
	"I don't enjoy making this."
}

function echo(msg,color3)
	CmdrClient.Events.AddLine(msg,color3)
end

--print welcome message
echo([[gv3u's Admin v0.2.1 "The Somewhat Large Update"]])
if math.random(1,99999) == 69420 then
	echo(seriousMessages[math.random(1,#seriousMessages)],Color3.fromRGB(255,0,0))
else
	echo(messages[math.random(1,#messages)],Color3.fromRGB(255,215,0))
end

--print void sb warning
if game.PlaceId == 843495510 then
	echo("WARNING: Using this script may get you banned from Void SB.",Color3.fromRGB(194,48,58))
end

echo("Type 'ls' for a list of scripts, or type 'help' for a list of commands.")
echo("Please remember that gv3u's Admin is alpha software.")

game:GetService("StarterGui"):SetCore("SendNotification",{Title="gv3u's Admin",Text="gv3u's Admin has loaded. Press Left Alt for the Cmdr GUI.",Button1="Ok I'll"})

warn("gv3u's Admin loaded succesfully.")
