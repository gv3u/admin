# Changelog:
## 	v0.1 "[NO CODENAME]" | 6/12/20:
-	Added basic functionality, migrated script pack to gv3u's Admin.
-	More commands are coming, obviously. Just wanted to get this uploaded.

##	v0.1.1 "Can you even call this an update?" | 6/14/20:
-	Added new MOTDs, and added Chronos to the scripts folder.
-	Removed notification so people don't think this is a Nebula's Stuff edit. (and it isn't lol)
-	Fixed a bug where the whitelist doesn't work when there is more than one person added. (STUPID, I KNOW)
-   The module is now obfuscated so skids can't change the whitelist.

##	v0.1.2 "gv3u doesn't test anything." | 6/15/20:
-	Changed Chronos to use EzConvert because I'm stupid and thought the old FakeMouse would work.
-	`,give` now checks if owner is an ObjectValue as well as a StringValue.
-	Added in an old favorite, Memeus v2.5, and also added Krystal Dance v1 and 2.
-	The kick command now has a "reason" parameter.
-	Changed the default kick/ban reason so they actually make sense.

##  v0.1.3 "So minor, even the recent TF2 updates have more content than this" | 6/19/20:
-   Fixed a bug in the Laser Pointer where multiple cubes could spawn.
-   Moved changelog and uploaded unobfuscated MainModule to GitLab.

## 6/21/20:
-   Made minor changes to SG.
-   Reuploaded deobfuscated MainModule to ROBLOX.
-   Uploaded command scripts to GitLab.
-   Made the GitLab repository public.

## v0.1.4 "gv3u is stupid." 6/25/20:
-   FINALLY fix the Whitelist issue.

## 6/26/20:
-   (Re)added UG.

## v0.2.0 "I'd almost say this is GOOD." 7/4/2020:
- Added `,time`, `,restrictscripts/,unrestrictscripts` and `,ec`.
- Created EpicGamerFunctions so I don't have to copy and paste one function several hundred times.
- The Cmdr Server module is now in ServerScriptService so EpicGamerFunctions can be accessed.
- Added some more HILARIOUS and EPIC messages which are totally not cringy.

## v0.2.1 "The Somewhat Large Update" 7/27/2020:
- Added `,plrd` and `,r6`.
- Added yet more HILARIOUS WACKY messages.
- I was planning to do more for this update, but it got so late and I was so tired that I decided to put it off. Next update will have some cool shit, including some original scripts that are NOT STOLEN FROM ANYTHING!!!

## 10|24|2020:
- Moved from GitLab to self-hosted Gitea instance (introvertGit).
- Removed redundant joke messages.